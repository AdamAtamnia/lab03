// Adam Atamnia 2036819
public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
    }
    public double dotProduct(Vector3d v3d){
        return this.x * v3d.getX() + this.y * v3d.getY() + this.z * v3d.getZ();
    }
    public Vector3d add(Vector3d v3d){
        return new Vector3d(this.x + v3d.getX(), this.y + v3d.getY(), this.z + v3d.getZ());
    }

}