// Adam Atamnia 2036819
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
    @Test
    public void testGetMethods() {
        //fail("this test will fail");
        Vector3d v3d = new Vector3d(2, 4, 6);
        assertEquals(v3d.getX(),2);
        assertEquals(v3d.getY(),4);
        assertEquals(v3d.getZ(),6);
    }

    @Test
    public void testMagnitude() {
        //fail("this test will fail");
        Vector3d v3d = new Vector3d(2, 4, 6);
        assertEquals(v3d.magnitude(), 7.4833147735, .00001); 
        
    }

    @Test
    public void testDotProduct() {
        //fail("this test will fail");
        Vector3d v3d = new Vector3d(2, 4, 6);
        Vector3d v3d2 = new Vector3d(6, 2, 4);
        assertEquals(v3d.dotProduct(v3d2), 44, .00001);
    }

    @Test
    public void testAdd() {
        //fail("this test will fail");
        Vector3d v3d = new Vector3d(2, 4, 6);
        Vector3d v3d2 = new Vector3d(6, 2, 4);
        Vector3d v3dResult = v3d.add(v3d2);
        
        assertEquals(v3dResult.getX(),8);
        assertEquals(v3dResult.getY(),6);
        assertEquals(v3dResult.getZ(),10);
        
    }

}
